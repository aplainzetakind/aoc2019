module Main where

import Advent19.Utils hiding (Parser, option)
import Advent19.Days
import Data.Void
import Data.Foldable
import Text.Megaparsec hiding (option)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer
import Options.Applicative
import Data.Semigroup ((<>))
import qualified Data.Map as M
import qualified Data.Set as S

data Day = Day Int | All deriving Show

data Star = One | Two | Both deriving Show

data Options = Options { getDays :: Day
                       , getStar :: Star
                       , getBig  :: Bool } deriving Show

type Parser' = Parsec Void String

solutions :: M.Map Int (String, (Solver, Solver, Solver))
solutions = M.fromList [ ( 1, ("01", (d01star1, d01star2, d01bothStars)))
                       , ( 2, ("02", (d02star1, d02star2, d02bothStars)))
                       , ( 3, ("03", (d03star1, d03star2, d03bothStars)))
                       , ( 4, ("04", (d04star1, d04star2, d04bothStars)))
                       , ( 5, ("05", (d05star1, d05star2, d05bothStars)))
                       , ( 6, ("06", (d06star1, d06star2, d06bothStars)))
                       , ( 7, ("07", (d07star1, d07star2, d07bothStars)))
                       , ( 8, ("08", (d08star1, d08star2, d08bothStars)))
                       , ( 9, ("09", (d09star1, d09star2, d09bothStars)))
                       , (10, ("10", (d10star1, d10star2, d10bothStars)))
                       , (11, ("11", (d11star1, d11star2, d11bothStars)))
                       , (12, ("12", (d12star1, d12star2, d12bothStars)))
                       , (13, ("13", (d13star1, d13star2, d13bothStars)))
                       , (14, ("14", (d14star1, d14star2, d14bothStars)))
                       -- , (15, ("15", (d15star1, d15star2, d15bothStars)))
                       -- , (16, ("16", (d16star1, d16star2, d16bothStars)))
                       -- , (17, ("17", (d17star1, d17star2, d17bothStars)))
                       -- , (18, ("18", (d18star1, d18star2, d18bothStars)))
                       -- , (19, ("19", (d19star1, d19star2, d19bothStars)))
                       -- , (20, ("20", (d20star1, d20star2, d20bothStars)))
                       -- , (21, ("21", (d21star1, d21star2, d21bothStars)))
                       -- , (22, ("22", (d22star1, d22star2, d22bothStars)))
                       -- , (23, ("23", (d23star1, d23star2, d23bothStars)))
                       -- , (24, ("24", (d24star1, d24star2, d24bothStars)))
                       -- , (25, ("25", (d25star1, d25star2, d25bothStars)))
                       ]

dayList :: Day
           -> [ (String, (Solver, Solver, Solver))]
dayList All = snd <$> M.toList solutions
dayList (Day n) = case M.lookup n solutions of
                    Just f  -> [f]
                    Nothing -> []

day :: Parser' Day
day = do n <- decimal <* eof
         if n >= 1 && n <= 25
            then pure $ Day n
            else parseError $ TrivialError 0 Nothing S.empty

star :: Parser' Star
star = try (const One <$> (single '1' *> eof))
       <|> (const Two <$> (single '2' *> eof))

readDay :: String -> Maybe Day
readDay s = case runParser day "" s of
              Right d -> Just d
              Left  _ -> Nothing

readStar :: String -> Maybe Star
readStar s = case runParser star "" s of
              Right d -> Just d
              Left  _ -> Nothing

options :: Parser Options
options = Options
          <$> option (maybeReader readDay)
              ( short 'd'
              <> help "Day number. Runs all days if omitted."
              <> showDefault
              <> value All
              <> metavar "N" )
          <*> option (maybeReader readStar)
              ( short 's'
              <> help "Star number. Runs both if omitted."
              <> showDefault
              <> value Both
              <> metavar "N" )
          <*> switch ( short 'b'
              <> help "Use big inputs." )

pickSolution :: Star -> (Solver, Solver, Solver) -> Solver
pickSolution One  (f, _, _) = f
pickSolution Two  (_, f, _) = f
pickSolution Both (_, _, f) = f

run :: Options -> IO ()
run (Options d s b)
        = if null (dayList d)
            then fail "Day not available."
            else traverse_ f . (fmap . fmap) (pickSolution s) $ dayList d
           where f (s, g)
                   = let (name1, name2) = if b then ("/biginput","/biginput2")
                                               else ("/input", "/input2")
                         path1          = "inputs/day" ++ s ++ name1
                         path2          = "inputs/day" ++ s ++ name2
                     in g path1 path2

main = run =<< execParser opts
  where
    opts = info (options <**> helper)
      ( fullDesc
     <> progDesc "Main executable for solutions to AoC2019"
     <> header "Advent of Code 2019" )
