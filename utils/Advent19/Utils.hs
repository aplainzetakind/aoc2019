module Advent19.Utils (module Advent19.Utils) where

import Advent19.Points2D
import System.FilePath as Advent19.Utils
import Data.Void
import Text.Megaparsec as Advent19.Utils
import Text.Megaparsec.Char as Advent19.Utils
import Text.Megaparsec.Char.Lexer
import Control.Monad.Combinators as Advent19.Utils
import Control.Monad (void)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Text.Printf
import Data.List

type Parser = Parsec Void T.Text

type Solver = FilePath -> FilePath -> IO ()

tmap :: (b -> c) -> (a, b) -> (a, c)
tmap f ~(a, b) = (a, f b)

safeHead :: [a] -> Maybe a
safeHead (x:_) = Just x
safeHead _     = Nothing

safeMinimum :: Ord a => [a] -> Maybe a
safeMinimum [] = Nothing
safeMinimum xs = Just $ minimum xs

toDigitsRev :: Int -> [Int]
toDigitsRev n | n <= 0 = []
              | n < 10 = [n]
              | otherwise = r : toDigitsRev s
                  where (s,r) = n `divMod` 10

announce :: Show a => Int -> Int -> a -> IO()
announce d s r = putStrLn $ printf "Day %02d star %1d: %s" d s (show r)

combineWith :: Monoid c => (b -> c -> c) -> Parser b -> Parser c
combineWith comb p = go id
              where go f = do m <- optional p
                              case m of
                                Nothing -> pure (f  mempty)
                                Just x  -> go (f . comb x)

int :: Integral a => Parser a
int = decimal

signedInt :: Integral a => Parser a
signedInt = signed (pure ()) decimal

minus :: Parser Char
minus = single '-'

direction :: Parser Direction
direction = const R <$> single 'R'
        <|> const U <$> single 'U'
        <|> const L <$> single 'L'
        <|> const D <$> single 'D'

move :: Parser Move
move = M <$> direction <*> int

comma :: Parser Char
comma = single ','

parens :: Parser a -> Parser a
parens p = single '(' *> p <* single ')'

parenCommaList :: Parser a -> Parser [a]
parenCommaList = parens . commaList

commaList :: Parser a -> Parser [a]
commaList p = p `sepBy` comma

pair :: Parser a -> Parser (a, a)
pair p = parens $ (,) <$> (p <* comma) <*> p

line :: Parser a -> Parser a
line p = p <* (void eol <|> eof)

lines_ :: Parser a -> Parser [a]
lines_ p = many (line p) <* eof

orbit :: Parser (T.Text, T.Text)
orbit = do p <- T.pack <$> many alphaNumChar
           single ')'
           o <- T.pack <$> many alphaNumChar
           (const () <$> eol) <|> eof
           pure (p, o)

parseFile :: Parser a -> FilePath -> IO a
parseFile p f = do t <- T.readFile f
                   case parse p "" t of
                     Left e  -> fail (errorBundlePretty e)
                     Right a -> pure a

parseFile' :: Parsec Void String a -> FilePath -> IO a
parseFile' p f = do t <- readFile f
                    case parse p "" t of
                      Left e  -> fail (errorBundlePretty e)
                      Right a -> pure a
