module Advent19.Points2D where

import Data.Tuple.Extra

data Direction = R | U | L | D deriving Show

data Move = M Direction Int deriving Show

type Point = (Int, Int)

type Segment = (Point, Point)

scale :: Int -> Point -> Point
scale s = both (* s)

componentwise :: (Int -> Int -> Int) -> Point -> Point -> Point
componentwise f (x, y) (x', y') = (f x x', f y y')

movePoint :: Direction -> Int -> Point -> Point
movePoint R d (x, y) = (x + d, y)
movePoint U d (x, y) = (x, y + d)
movePoint L d (x, y) = (x - d, y)
movePoint D d (x, y) = (x, y - d)

distanceM :: Point -> Point -> Int
distanceM (a, b) (c, d) = abs (a - c) + abs (b - d)

normM :: Point -> Int
normM = distanceM (0,0)
