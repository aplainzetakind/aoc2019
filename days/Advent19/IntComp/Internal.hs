{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Advent19.IntComp.Internal ( Computer ()
                                 , CompState ()
                                 , initializeState
                                 , getIP
                                 , modifyIP
                                 , getOffset
                                 , modifyOffset
                                 , popIn
                                 , output
                                 , readA
                                 , writeA
                                 , runC
                                 ) where

-- import Control.Monad.ST.Lazy (ST, runST)
import Control.Monad.Trans.Reader (ReaderT, runReaderT, ask)
import Control.Monad.Trans.State (StateT, evalStateT, get, put)
import Control.Monad.Trans.Class (lift)
import Data.Array.IO ( IOUArray
                     , newListArray
                     , readArray
                     , writeArray
                     , thaw
                     , getBounds
                     , getElems )
import Data.Array (Array, listArray)
import Conduit (ConduitT, runConduit, yield)
import Data.Conduit.Lazy (lazyConsume)
import System.IO.Unsafe (unsafePerformIO)
import Debug.Trace

-- |To switch between ST and IO arrays.
type ArrayType s = IOUArray
type ArrayMonad s = IO

type Array' s = (ArrayType s) Int Int

instance Show (Array' s) where
  show = show . zipWith (,) [0..] . unsafePerformIO . getElems

data CompState s = CState { _IP     :: Int
                          , _offset :: Int
                          , _ins    :: [Int]
                          , _mem    :: Array' s
                          } deriving Show

initializeState :: [Int] -> Array' s -> CompState s
initializeState = CState 0 0
newtype Computer s a
  = Computer
     { getComputer :: StateT (CompState s) (ConduitT () Int (ArrayMonad s)) a
            } deriving (Functor, Applicative, Monad)

runC :: Computer s () -> CompState s -> IO [Int]
runC comp cs = lazyConsume $ getComputer comp `evalStateT` cs

getMem :: Computer s (Array' s)
getMem = _mem <$> getCompState

putMem :: Array' s -> Computer s ()
putMem a = do s <- getCompState
              putCompState $ s { _mem = a }

maxAddr :: Computer s Int
maxAddr = Computer . lift . lift . fmap snd . getBounds =<< getMem

extendArray :: Array' s -> Int -> Computer s ()
extendArray a p = do a' <- Computer . lift . lift
                            $ do elems <- getElems a
                                 newListArray (0, p) $ elems ++ repeat 0
                     putMem a'

writeA :: Int {- ^ position -}
       -> Int {- ^ value -}
       -> Computer s ()
writeA p n | p < 0 = fail "writeA: Negative address."
writeA p n = do a <- getMem
                m <- maxAddr
                if p <= m
                  then Computer $ lift . lift $ writeArray a p n
                  else extendArray a p >> writeA p n

readA :: Int {- ^ position -}
       -> Computer s Int
readA p | p < 0 = fail "readA: Negative address."
readA p = do a <- getMem
             m <- maxAddr
             if p <= m
               then Computer $ lift . lift $ readArray a p
               else pure 0

getCompState :: Computer s (CompState s)
getCompState = Computer get

putCompState :: CompState s -> Computer s ()
putCompState s = Computer (put s)

getIP :: Computer s Int
getIP = _IP <$> getCompState

putIP :: Int -> Computer s ()
putIP n = do s <- getCompState
             putCompState $ s { _IP = n }

modifyIP :: (Int -> Int) -> Computer s ()
modifyIP f = getIP >>= putIP . f

getOffset :: Computer s Int
getOffset = _offset <$> getCompState

putOffset :: Int -> Computer s ()
putOffset n = do s <- getCompState
                 putCompState $ s { _offset = n }

modifyOffset :: (Int -> Int) -> Computer s ()
modifyOffset f = getOffset >>= putOffset . f

getIns :: Computer s [Int]
getIns = _ins <$> getCompState

putIns :: [Int] -> Computer s ()
putIns ns = do s <- getCompState
               putCompState $ s { _ins = ns }

modifyIns :: ([Int] -> [Int]) -> Computer s ()
modifyIns f = getIns >>= putIns . f

popIn :: Computer s (Maybe Int)
popIn = do ns <- getIns
           case ns of
             x : xs -> putIns xs >> pure (Just x)
             []     -> pure Nothing

output :: Int -> Computer s ()
output n = Computer $ lift $ yield n
