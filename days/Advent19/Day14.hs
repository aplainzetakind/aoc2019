{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Advent19.Day14 where

import Advent19.Utils
import Control.Monad
import Control.Arrow
import Control.Monad.Reader
import qualified Data.Map as M
import qualified Data.Set as S
import Debug.Trace
import Data.Tuple.Extra (both)

announce' :: Show a => Int -> a -> IO ()
announce' = announce 14

parseInput :: FilePath -> IO [Reaction]
parseInput = parseFile (lines_ reaction)

resource :: Parser Resource
resource = do q <- int
              space
              str <- many alphaNumChar
              let c = if str == "ORE" then Ore else Chem str
              pure (c, q)

reaction :: Parser Reaction
reaction = do rs <- resource `sepBy` string ", "
              space
              string "=>"
              space
              (c, q) <- resource
              pure (c, (q, rs))

data Chemical = Chem String | Ore deriving (Eq, Ord, Show)

type Resource = (Chemical, Int)

type Requirements = [Resource]

type Reaction = (Chemical, (Int, Requirements))

type ReactionMap = M.Map Chemical (Int, Requirements)

newtype Reactor a = Reactor { unReactor :: Reader ReactionMap a }
                deriving (Functor, Applicative, Monad, MonadReader ReactionMap)

runReactor :: Reactor a -> ReactionMap -> a
runReactor r m = (`runReader` m) $ unReactor r

lookupChem :: Chemical -> Reactor (Maybe (Int, Requirements))
lookupChem c = asks $ M.lookup c

fuel :: Int -> Resource
fuel n = (Chem "FUEL", n)

reduceRequirement :: Resource -> Reactor ([Resource], Maybe Resource)
reduceRequirement r@(c, q)
    = do mress <- lookupChem c
         case mress of
            Nothing         -> pure ([r], Nothing)
            Just (q', ress) -> pure (ress', excess)
                  where (s, excess) = let (d, r) = q `divMod` q'
                                      in if r == 0
                                          then (d, Nothing)
                                          else (d + 1, Just (c, q' - r))
                        ress'       = (fmap . fmap) (* s) ress


fuelCost :: Int -> Reactor Int
fuelCost n = go (uncurry M.singleton $ fuel n) M.empty
  where go reqs exss
          = case M.deleteFindMin reqs of
              ((Ore, _), _) -> do e <- reduceExcesses exss
                                  pure $ subtract e cost
                                   where cost = sum . M.elems $ reqs
              (r, m)        -> do (rs, mr) <- reduceRequirement r
                                  let reqs' = M.unionWith (+) m
                                                $ M.fromList rs
                                      exss' = case mr of
                                                Nothing -> exss
                                                Just (c, q)
                                                        -> M.insertWith (+)
                                                                c q exss
                                  go reqs' exss'

isOre :: Resource -> Reactor Bool
isOre (c, q) = pure (c == Ore)

reduceExcess :: Resource -> Reactor [Resource]
reduceExcess r@(c, q) = do rs <- lookupChem c
                           case rs of
                             Just (q', rs')
                                 -> if q >= q'
                                      then pure rs''
                                      else pure [r]
                                        where (s, q'') = q `divMod` q'
                                              rs'' = (c, q'')
                                                     : (fmap (* s) <$> rs')
                             Nothing -> pure [r]

reduceExcesses :: M.Map Chemical Int -> Reactor Int
reduceExcesses m = do m' <- M.fromListWith (+) . concat
                                        <$> traverse reduceExcess (M.toList m)
                      if m == m'
                        then do l <- filterM isOre . M.toList $ m'
                                pure . sum . fmap snd $ l
                        else reduceExcesses m'

maxFuelWithCapacity :: Int -> Reactor Int
maxFuelWithCapacity n
    = do unit <- fuelCost 1
         let maximize = fst . last . takeWhile ((<= n) . snd)
             m = n `div` unit
         go m 0
           where go i 0 = do c <- fuelCost (i + 1)
                             if c > n then pure i
                                      else go i 1
                 go i k = do c <- fuelCost (i + 2 ^ k)
                             if c > n then go (i + 2 ^ (k - 1)) 0
                                      else go i (k + 1)

d14star1 :: Solver
d14star1 f _ = M.fromList <$> parseInput f
                  >>= announce' 1 . runReactor (fuelCost 1)

d14star2 :: Solver
d14star2 f _ = M.fromList <$> parseInput f
                  >>= announce' 1
                      . runReactor (maxFuelWithCapacity 1000000000000)

d14bothStars :: Solver
d14bothStars f f' = d14star1 f f' >> d14star2 f f'
