module Advent19.Day03 where

import Advent19.Utils
import Advent19.Points2D
import Data.List
import qualified Data.Set as S
import qualified Data.Map as M

type Delay = Int

-- Probably better to get the delays while parsing.

announce' :: Show a => Int -> a -> IO ()
announce' = announce 3

parseInput :: FilePath -> IO ([Move], [Move])
parseInput = parseFile $ (,) <$> line (commaList move)
                             <*> line (commaList move)

intersectSegments :: Segment -> Segment -> [Point]
intersectSegments ((x1, y1), (x1', y1')) ((x2, y2), (x2', y2'))
  = (,) <$> [xm..xM] <*> [ym..yM]
    where xm = max x1m x2m
          xM = min x1M x2M
          ym = max y1m y2m
          yM = min y1M y2M
          x1m = min x1 x1'
          x2m = min x2 x2'
          x1M = max x1 x1'
          x2M = max x2 x2'
          y1m = min y1 y1'
          y2m = min y2 y2'
          y1M = max y1 y1'
          y2M = max y2 y2'

intersectSegmentsWithDelay :: (Segment, Delay)
                              -> (Segment, Delay)
                              -> [(Point, Delay)]
intersectSegmentsWithDelay (s1, d1) (s2, d2)
  = f <$> intersectSegments s1 s2
      where f p@(a, b) = let d = 2 + d1 + d2
                                   + abs (a - x1) + abs (a - x2)
                                   + abs (b - y1) + abs (b - y2)

                         in (p, d)
            x1 = fst . fst $ s1
            x2 = fst . fst $ s2
            y1 = snd . fst $ s1
            y2 = snd . fst $ s2

moveToSegment :: Point -> Move -> Segment
moveToSegment p (M d l) = (movePoint d 1 p, movePoint d l p)

movesToSegments :: [Move] -> [Segment]
movesToSegments = snd . foldl' f ((0,0), [])
                      where f (p, s) m = let seg@(p1, p2) = moveToSegment p m
                                         in (p2, seg : s)

movesToSegmentsWithDelay :: [Move] -> [(Segment, Delay)]
movesToSegmentsWithDelay = snd . foldl' f (((0,0),0), [])
                      where f ((p, d), s) m@(M _ l)
                              = let seg@(p1, p2) = moveToSegment p m
                                in ((p2, d + l), (seg, d) : s)

d03star1 :: Solver
d03star1 f _ = do (cable1, cable2) <- parseInput f
                  let s1  = movesToSegments cable1
                      s2  = movesToSegments cable2
                      is  = concat $ intersectSegments <$> s1 <*> s2
                      res = minimum $ normM <$> is
                  announce' 1 res

d03star2 :: Solver
d03star2 f _ = do (cable1, cable2) <- parseInput f
                  let s1  = movesToSegmentsWithDelay cable1
                      s2  = movesToSegmentsWithDelay cable2
                      is  = concat $ intersectSegmentsWithDelay <$> s1 <*> s2
                      res = minimum $ snd <$> is
                  announce' 2 res

d03bothStars :: Solver
d03bothStars f _ = do (cable1, cable2) <- parseInput f
                      let s1  = movesToSegmentsWithDelay cable1
                          s2  = movesToSegmentsWithDelay cable2
                          is  = concat
                                $ intersectSegmentsWithDelay <$> s1 <*> s2
                          res1 = minimum $ normM . fst <$> is
                          res2 = minimum $ snd <$> is
                      announce' 1 res1
                      announce' 2 res2
