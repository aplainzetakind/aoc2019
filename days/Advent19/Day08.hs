module Advent19.Day08 where

import Advent19.Utils
import Data.List.Extra
import Data.Array.Unboxed
import Control.Monad.Reader

-- Maybe do with mutable arrays.

-- |We would like to be able to work with images of different sizes.
type Dimensions = (Int, Int)

announce' :: Show a => Int -> a -> IO ()
announce' = announce 8

parseInput :: FilePath -> IO String
parseInput f = takeWhile (/= '\n') <$> readFile f

parseInput2 :: FilePath -> IO Dimensions
parseInput2 = parseFile (pair int)

countOf :: Eq a => a -> [a] -> Int
countOf x = length . filter (== x)

size :: Reader Dimensions Int
size = asks $ uncurry (*)

width :: Reader Dimensions Int
width = asks fst

star1 :: Reader Dimensions (String -> Int)
star1 = do s <- size
           pure $ ((*) <$> countOf '1' <*> countOf '2')
                    . minimumOn (countOf '0') . chunksOf s

overlay :: Char -> Char -> Char
overlay '2' x = x
overlay  x  _ = x

star2 :: Reader Dimensions (String -> String)
star2 = do s <- size
           w <- width
           f <- visualize
           pure $ f . accumArray overlay '2' (0, s - 1)
                        . zip (cycle [0..s - 1])

visualize :: Reader Dimensions (UArray Int Char -> String)
visualize = do w <- width
               pure $ unlines . chunksOf w . map render . elems
                   where render '0' = ' '
                         render '1' = '▓'
                         render  x  =  x

d08star1 :: Solver
d08star1 f f' = do lst  <- parseInput f
                   dims <- parseInput2 f'
                   announce' 1 $ runReader star1 dims lst

d08star2 :: Solver
d08star2 f f' = do lst  <- parseInput f
                   dims <- parseInput2 f'
                   putStrLn "Day 08 star 2:"
                   putStr $ runReader star2 dims lst

d08bothStars :: Solver
d08bothStars f f' = d08star1 f f' >> d08star2 f f'
