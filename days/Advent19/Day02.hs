module Advent19.Day02 where

import Advent19.Utils ( Solver -- ^ FilePath -> FilePath -> IO ()
                      , announce
                      , line
                      , commaList
                      , int
                      , parseFile )
import Data.List
import Data.Array.IO

announce' :: Show a => Int -> a -> IO ()
announce' = announce 2

-- |Make it easy to switch between Int and Integer arrays.
type Int' = Int
type Array' = IOUArray Int Int'

-- |Parse input into an [Int'].
parseProg :: FilePath -> IO [Int']
parseProg = parseFile (line (commaList int))

listToArray :: [Int'] -> IO Array'
listToArray [] = error "Empty list."
listToArray xs = newListArray (0, l) xs where l = length xs - 1

-- |Takes the program array and mutates the array until the program halts.
runProgram :: Int {- ^ array length, to check for out of bound jumps -}
              -> Array'
              -> IO ()
runProgram bound a = go 0
        where go :: Int -> IO ()
              go pos = if pos > bound
                         then fail "Jump out of bounds."
                         else do inst <- readArray a pos
                                 if inst == 99
                                   then pure ()
                                   else do execute inst pos a
                                           go (pos + 4)

-- |Runs a given instruction at a given position.
execute :: Int' {- ^ instruction -}
           -> Int {- ^ position -}
           -> Array'
           -> IO ()
execute inst pos a
    = case inst of
          1 -> operate (+)
          2 -> operate (*)
          _ -> fail "Unrecognized instruction."
        where operate :: (Int' -> Int' -> Int') -> IO ()
              operate f
               = do arg1 <- readArray a . fromIntegral =<< readArray a (pos + 1)
                    arg2 <- readArray a . fromIntegral =<< readArray a (pos + 2)
                    trgt <- fromIntegral <$> readArray a (pos + 3)
                    writeArray a trgt (f arg1 arg2)

-- |Run the program after writing the given input over positions 1 and 2.
runInput :: Int' -> Int' -> Array' -> IO Int'
runInput n v a = do (_, bound) <- getBounds a
                    writeArray a 1 n
                    writeArray a 2 v
                    runProgram bound a
                    readArray a 0

inputHash :: Int' -> Int' -> Int'
inputHash n v = 100 * n + v

runInput' :: Array' -> Int' -> Int' -> IO Int'
runInput' a n v = runInput n v =<< mapArray id a

d02star1 :: Solver
d02star1 f _ = announce' 1
               =<< runInput 12 2
               =<< listToArray
               =<< parseProg f

-- |Uses saddleback search based on the observation that all the entries of the
-- array are monotone increasing in both inputs. Starting at the bottom left
-- corner of the matrix, we search by moving up if the result is too large and
-- left if it is too small.
d02star2 :: Solver
d02star2 f f' = do target <- parseFile (line int) f'
                   a      <- listToArray =<< parseProg f
                   let go m n | m < 0 || n < 0 || m > 99 || n > 99
                                 = fail "Target not attained."
                       go m n
                          = do res <- runInput' a m n
                               case compare res target of
                                 LT -> go m (n + 1)
                                 GT -> go (m - 1) n
                                 EQ -> announce' 2 $ inputHash m n
                   go 99 0

d02bothStars :: Solver
d02bothStars f f' = d02star1 f f' >> d02star2 f f'

-- The code below is for a naive brute force solution.

d02star2' :: Solver
d02star2' f f' = do target <- parseFile (line int) f'
                    a      <- listToArray =<< parseProg f
                    r  <- fmap (elemIndex target)
                          $ sequence $ runInput' a <$> [0..99] <*> [0..99]
                    case r of
                      Nothing -> fail "Target not attained."
                      Just i  -> announce' 2 i
