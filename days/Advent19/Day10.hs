module Advent19.Day10 where

import Advent19.Utils (Solver, announce)
import Advent19.Points2D (Point, distanceM, componentwise, scale)
import Control.Monad.Reader (Reader, runReader, asks)
import Control.Monad ((<=<))
import Data.PSQueue (PSQ, Binding (..))
import qualified Data.PSQueue as PSQ
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import Data.List.Extra (maximumOn)
import Control.Arrow ((&&&))

announce' :: Show a => Int -> a -> IO ()
announce' = announce 10

data Bounds = Bounds { _maxX :: Int
                     , _maxY :: Int
                     }

data Space = Space { _asteroids :: S.Set Point, _bounds :: Bounds }

getSpace :: String -> Space
getSpace str = let s  = S.fromList . concat
                        . zipWith parseLine [0..] . lines $ str
                   xM = S.findMax $ S.map fst s
                   yM = S.findMax $ S.map snd s
               in Space s (Bounds xM yM)

parseLine :: Int -> String -> [Point]
parseLine n = fmap ((, n) . fst) . filter ((== '#') . snd) . zip [0..]

parseInput :: FilePath -> IO Space
parseInput = pure . getSpace <=< readFile

data Direction = Dir Int Int

instance Eq Direction where
  Dir 0 y1 == Dir 0 y2 = signum y1 == signum y2
  (Dir x1 y1) == (Dir x2 y2) = x1 * y2 == x2 * y1 && x1 * x2 > 0

instance Ord Direction where
  Dir 0 y <= _           | y  > 0 = True
  Dir 0 y <= Dir 0 y'    | y' > 0 = False
  Dir 0 y <= Dir x1 _             = x1 <= 0
  _       <= Dir 0 y     | y > 0  = False
  Dir x y <= Dir 0 y'             = x > 0
  Dir x1 y1 <= Dir x2 y2 | signum x1 == signum x2 = x2 * y1 <= x1 * y2
                         | otherwise = signum x1 > signum x2

toDirection :: Point -> Direction
toDirection (0, 0) = error "The origin has no direction."
toDirection (x, y) = Dir x y

type DirDistMap = M.Map Direction (PSQ Point Int)

distanceQueueMapOfDirections :: Point -> Reader Space DirDistMap
distanceQueueMapOfDirections p0
     = do as  <- asks (S.elems . S.delete p0
                               . _asteroids)
          let dirList = fmap (\a -> (f a, [a])) as
              f       = toDirection
                        . componentwise subtract p0
              dirGrp  = M.fromListWith (++) dirList
              ps2q    = foldr (\p -> PSQ.insert p (g p))
                              PSQ.empty
              g       = distanceM p0
          pure $ fmap ps2q dirGrp

maximalAsteroidSighting :: Reader Space (Point, DirDistMap)
maximalAsteroidSighting
    = do as   <- asks $ S.elems . _asteroids
         mpqs <- traverse (sequence . (id &&& distanceQueueMapOfDirections)) as
         pure $ maximumOn (M.size . snd) mpqs

shotList :: DirDistMap -> Reader Space [Point]
shotList = go id M.empty
    where go :: ([Point] -> [Point]) -> M.Map Direction (PSQ Point Int)
                    -> M.Map Direction (PSQ Point Int) -> Reader Space [Point]
          go f md mn | M.null mn = if M.null md
                                     then pure (f [])
                                     else go f mn md
                     | otherwise = let ((dir, q), mn') = M.deleteFindMin mn
                                       Just (p :-> dist, q') = PSQ.minView q
                                       md' = if PSQ.null q'
                                              then md
                                              else M.insert dir q' md
                                   in go (f . (p :)) md' mn'

star2hash :: Point -> Int
star2hash (x, y) = 100 * x + y

d10star1 :: Solver
d10star1 f _ = do s <- parseInput f
                  announce' 1 . M.size . snd
                                $ runReader maximalAsteroidSighting s

d10star2 :: Solver
d10star2 f _ = do s <- parseInput f
                  let ps = (`runReader` s)
                              (shotList . snd
                                        =<< maximalAsteroidSighting)
                  announce' 2 . star2hash $ ps !! 199

d10bothStars :: Solver
d10bothStars f _ = do s <- parseInput f
                      let (s1, s2)
                           = (`runReader` s)
                               $ do (p0, m) <- maximalAsteroidSighting
                                    ps <- shotList m
                                    pure (M.size m, ps !! 199)
                      announce' 1 s1
                      announce' 2 . star2hash $ s2
