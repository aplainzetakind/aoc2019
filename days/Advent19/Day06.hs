{-# LANGUAGE PatternGuards #-}
module Advent19.Day06 where

import Advent19.Utils
import qualified Data.Set as S
import qualified Data.Map as M
import Control.Monad ((<=<))
import Data.Foldable
import Data.Monoid
import Data.Maybe
import Data.List.Extra (stripInfix)
import Data.Tuple.Extra
import qualified Data.Text as T

-- Use a PSQueue, where keys are planets and the priorities are their centers,
-- then derive a PSQ Planet (Int, Planet), where Int is the depth of the planet.
-- Then summing all the ints give star1, and star2 is obtained by fist ascending
-- to the common level then walking up together until arriving at the same
-- planet.

type Planet = T.Text

data OrbitTree a = Single a
                   | Tree (S.Set (OrbitTree a)) a
                   deriving (Eq, Ord, Show)

getRoot :: OrbitTree a -> a
getRoot (Single a) = a
getRoot (Tree _ a) = a

announce' :: Show a => Int -> a -> IO ()
announce' = announce 6

parseInput :: FilePath -> IO [(Planet, [Planet])]
parseInput = pure . fmap (fmap pure . both T.pack) . catMaybes . fmap (stripInfix ")") . lines <=< readFile

-- parseInput :: FilePath -> IO (M.Map Planet [Planet])
-- parseInput = parseFile p
  -- where p = combineWith (\(p, o) -> M.insertWith (++) p [o]) orbit

-- |For star 1, we don't need the tree and can directly work with the parsed
-- Map.
countOrbits :: M.Map Planet [Planet] -> Int
countOrbits m = getSum $ go (Sum 0) "COM"
                  where go :: Sum Int -> Planet -> Sum Int
                        go n p = let s = concat $ maybeToList $ M.lookup p m
                                 in n + foldMap (go (n + 1)) s

depthWeightedVertexCount :: OrbitTree (S.Set Planet) -> Int
depthWeightedVertexCount t = getSum $ go (Sum 0) t
                  where go n (Single _) = n
                        go n (Tree s _) = n + foldMap (go (n + 1)) s

buildOrbitTree :: Planet -> M.Map Planet [Planet] -> OrbitTree (S.Set Planet)
buildOrbitTree p m = case M.lookup p m of
                      Nothing -> Single $ S.singleton p
                      Just ps -> Tree t ps'
                        where t   = S.fromList tl
                              ps' = S.insert p
                                    $ foldr S.union S.empty $ getRoot <$> tl
                              tl  = (`buildOrbitTree` m) <$> ps

lowestCommonAncestor :: OrbitTree (S.Set Planet) -> Planet -> Planet
                        -> Maybe (OrbitTree (S.Set Planet))
lowestCommonAncestor t@(Single p) q q' | p == S.singleton q && q == q' = Just t
lowestCommonAncestor (Single _)   _ _                                  = Nothing
lowestCommonAncestor t@(Tree ss s) q q'
    | Just t' <- find ((qs `S.isSubsetOf`) . getRoot) ss
        = lowestCommonAncestor t' q q'
    | qs `S.isSubsetOf` s
        = Just t
    | otherwise
        = Nothing
  where qs = S.fromList [q,q']

depthIn :: OrbitTree (S.Set Planet) -> Planet -> Maybe Int
depthIn (Single s) p | s == S.singleton p = Just 0
                     | otherwise          = Nothing
depthIn (Tree ss s) p = find ((p `S.member`) . getRoot) ss
                        >>= fmap (+1) . (`depthIn` p)

d06star1 :: Solver
d06star1 f _ = parseInput f >>= announce' 1 . countOrbits . M.fromListWith (++)

d06star2 :: Solver
d06star2 f _ = do t <- buildOrbitTree "COM" . M.fromListWith (++)
                        <$> parseInput f
                  let n = do a <- lowestCommonAncestor t "SAN" "YOU"
                             s <- depthIn a "SAN"
                             y <- depthIn a "YOU"
                             pure $ s + y - 2
                  case n of
                    Nothing -> fail "Orbit transfer failed."
                    Just n' -> announce' 2 n'

d06bothStars :: Solver
d06bothStars f _ = do t <- buildOrbitTree "COM" . M.fromListWith (++)
                        <$> parseInput f
                      let n1 = depthWeightedVertexCount t
                          n2 = do a <- lowestCommonAncestor t "SAN" "YOU"
                                  s <- depthIn a "SAN"
                                  y <- depthIn a "YOU"
                                  pure $ s + y - 2
                      announce' 1 n1
                      case n2 of
                        Nothing -> fail "Orbit transfer failed."
                        Just n' -> announce' 2 n'
