{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Advent19.Day09 where

import Prelude hiding (LT, EQ)

import Advent19.Utils hiding (getOffset)
import Advent19.IntComp (runIntCompIOL)

announce' :: Show a => Int -> a -> IO ()
announce' = announce 9

parseInput :: FilePath -> IO [Int]
parseInput = parseFile (line (commaList signedInt))

d09star1 :: Solver
d09star1 f _ = do a <- parseInput f
                  r <- head <$> runIntCompIOL [1] a
                  announce' 1 r

d09star2 :: Solver
d09star2 f _ = do a <- parseInput f
                  r <- head <$> runIntCompIOL [2] a
                  announce' 1 r

d09bothStars :: Solver
d09bothStars f f' = d09star1 f f' >> d09star2 f f'
