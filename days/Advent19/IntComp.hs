module Advent19.IntComp ( unsafeRunIntComp
                        , unsafeRunIntCompL
                        , runIntCompIO
                        , runIntCompIOL
                        , makeArray) where

import Prelude hiding (LT, EQ)

import Advent19.Utils hiding (getOffset)
import Advent19.IntComp.Internal
import Data.Array (Array, listArray)
import Data.Array.MArray (thaw)
import System.IO.Unsafe (unsafePerformIO)
import Control.Monad ((<=<))

makeArray :: [Int] -> Array Int Int
makeArray l = listArray (0, length l - 1) l

data Mode = Literal | Pointer | Offset deriving Show

readMode :: Int -> Either ErrorMessage Mode
readMode 0 = Right Pointer
readMode 1 = Right Literal
readMode 2 = Right Offset
readMode x = Left $ "readMode: Unrecognized mode: " ++ show x

writeMode :: Int -> Either ErrorMessage Mode
writeMode 0 = Right Pointer
writeMode 1 = Left "writeMode: Can't write to literals."
writeMode 2 = Right Offset
writeMode x = Left $ "writeMode: Unrecognized mode: " ++ show x

data Instruction = ADD  Mode Mode Mode
                 | MUL  Mode Mode Mode
                 | IN   Mode
                 | OUT  Mode
                 | JMPT Mode Mode
                 | JMPF Mode Mode
                 | LT   Mode Mode Mode
                 | EQ   Mode Mode Mode
                 | OFST Mode
                 | HALT deriving Show

type ErrorMessage = String

decodeInstruction :: Int -> Either ErrorMessage Instruction
decodeInstruction n
      = case digits {-reversed -} of
            [1,0,x,y,z] -> ADD  <$> readMode  x <*> readMode y <*> writeMode z
            [2,0,x,y,z] -> MUL  <$> readMode  x <*> readMode y <*> writeMode z
            [3,0,x,0,0] -> IN   <$> writeMode x
            [4,0,x,0,0] -> OUT  <$> readMode  x
            [5,0,x,y,z] -> JMPT <$> readMode  x <*> readMode y
            [6,0,x,y,z] -> JMPF <$> readMode  x <*> readMode y
            [7,0,x,y,z] -> LT   <$> readMode  x <*> readMode y <*> writeMode z
            [8,0,x,y,z] -> EQ   <$> readMode  x <*> readMode y <*> writeMode z
            [9,0,x,0,0] -> OFST <$> readMode  x
            [9,9,0,0,0] -> Right HALT
            xs          -> error $ "decodeInstruction: Encountered "
                                      ++ (show =<< reverse xs)
         where digits = take 5 (toDigitsRev n ++ repeat 0)

readArgument :: Int -> Mode -> Computer s Int
readArgument o m = do ip <- getIP
                      v  <- readA (ip + o)
                      case m of
                        Pointer -> readA v
                        Literal -> pure v
                        Offset  -> readA . (v +) =<< getOffset

getWriteAddress :: Int -> Mode -> Computer s Int
getWriteAddress o m = do a <- readArgument o Literal
                         (+ a) <$> case m of
                                 Pointer -> pure 0
                                 Offset  -> getOffset
                                 Literal -> fail $ "getWriteAddress: Can't "
                                                   ++ "write in literal mode."

writeParam :: Mode
           -> Int {- ^ read offset -}
           -> Int {- ^ value to write -}
           -> Computer s ()
writeParam m o n = do a <- readArgument o Literal
                      wo <- case m of
                              Pointer   -> pure 0
                              Offset    -> getOffset
                              Literal   -> fail $ "writeParam: Can't write in "
                                                ++ "literal mode."
                      writeA (wo + a) n

intComp :: Computer s ()
intComp = do ip   <- getIP
             inst <- readA ip
             case decodeInstruction inst of
               Right i -> runInstruction i
               Left m  -> fail $ "Encountered " ++ m
                                                ++ " at position " ++ show ip

ternaryInst :: (Int -> Int -> Int) -> Mode -> Mode -> Mode -> Computer s ()
ternaryInst f m1 m2 m3 = do a1 <- readArgument 1 m1
                            a2 <- readArgument 2 m2
                            ad <- getWriteAddress 3 m3
                            writeA ad $ f a1 a2
                            modifyIP (+4)

jumpIf :: (Int -> Bool) -> Mode -> Mode -> Computer s ()
jumpIf f m1 m2 = do a1 <- readArgument 1 m1
                    a2 <- readArgument 2 m2
                    let g = if f a1 then const a2 else (+ 3)
                    modifyIP g

runInstruction :: Instruction -> Computer s ()
runInstruction (ADD m1 m2 m3) = ternaryInst (+) m1 m2 m3 >> intComp
runInstruction (MUL m1 m2 m3) = ternaryInst (*) m1 m2 m3 >> intComp
runInstruction (IN m)         = do i <- popIn
                                   ad <- getWriteAddress 1 m
                                   case i of
                                      Just n  -> do writeA ad n
                                                    modifyIP (+ 2)
                                                    intComp
                                      Nothing -> runInstruction HALT
runInstruction (OUT m)        = do a <- readArgument 1 m
                                   output a
                                   modifyIP (+ 2)
                                   intComp
runInstruction (JMPT m1 m2)   = jumpIf (/= 0) m1 m2 >> intComp
runInstruction (JMPF m1 m2)   = jumpIf (== 0) m1 m2 >> intComp
runInstruction (LT m1 m2 m3)  = do a1 <- readArgument 1 m1
                                   a2 <- readArgument 2 m2
                                   let r = if a1 < a2 then 1 else 0
                                   writeParam m3 3 r
                                   modifyIP (+ 4)
                                   intComp
runInstruction (EQ m1 m2 m3)  = do a1 <- readArgument 1 m1
                                   a2 <- readArgument 2 m2
                                   let r = if a1 == a2 then 1 else 0
                                   writeParam m3 3 r
                                   modifyIP (+ 4)
                                   intComp
runInstruction (OFST m)       = do a <- readArgument 1 m
                                   o <- getOffset
                                   modifyOffset (+ a)
                                   modifyIP (+ 2)
                                   intComp
runInstruction HALT           = pure ()

unsafeRunIntComp :: [Int] -> Array Int Int -> [Int]
unsafeRunIntComp is = unsafePerformIO . runIntCompIO is

runIntCompIO :: [Int] -> Array Int Int -> IO [Int]
runIntCompIO is = runC intComp . initializeState is <=< thaw

runIntCompIOL :: [Int] -> [Int] -> IO [Int]
runIntCompIOL is = runIntCompIO is . makeArray

unsafeRunIntCompL :: [Int] -> [Int] -> [Int]
unsafeRunIntCompL is = unsafeRunIntComp is . makeArray
