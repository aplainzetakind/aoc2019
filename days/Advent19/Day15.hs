{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Advent19.Day15 where

import Advent19.Utils hiding (State)
import Advent19.Points2D hiding (movePoint)
import Advent19.IntComp
import Control.Monad.Fail
import Control.Monad.Identity
import Control.Monad.State.Lazy
import Data.List
import Data.List.Extra
import qualified Data.Map as M
import qualified Data.Set as S
import Lens.Micro
import Lens.Micro.Mtl
import Lens.Micro.TH
import Debug.Trace

data Tile = Wall | Blank | O2 deriving Eq

data Territory = Territory { _known      :: M.Map Point Tile
                           , _unexplored :: S.Set Point }

makeLenses ''Territory

data ExplorationState = ES { _territory    :: Territory
                           , _location     :: Point
                           , _inputBuilder :: [Int] -> [Int]
                           , _outputs      :: [Int] }

makeLenses ''ExplorationState

newtype Exploration a
          = Exploration { _getExploration :: State ExplorationState a}
           deriving (Functor, Applicative, Monad)

makeLenses ''Exploration

execExploration :: Exploration a -> ExplorationState -> ExplorationState
execExploration e = execState (view getExploration e)

putInput :: Int -> Exploration ()
putInput n = Exploration $ inputBuilder %= (. (n :))

popOutput :: Exploration (Maybe Int)
popOutput = Exploration $ do mo <- use (outputs . to safeHead)
                             outputs %= tail
                             trace ("popped " ++ show mo) (pure ())
                             pure mo

alwaysNorth :: Exploration ()
alwaysNorth = putInput 1 >> loop
        where loop = do mo <- popOutput
                        case mo of
                          Nothing -> pure ()
                          Just o  -> putInput o >> loop

test1 = execExploration alwaysNorth (initialExplorationState [1..10])
          ^. inputBuilder . to ($ [])

popOutput' :: Exploration Int
popOutput' = Exploration $ do mo <- use (outputs . to safeHead)
                              outputs %= tail
                              trace ("popped " ++ show mo) (pure ())
                              case mo of
                                Nothing -> error "No status response."
                                Just o  -> pure o

alwaysNorth' :: Exploration ()
alwaysNorth' = putInput 1 >> loop
        where loop = popOutput' >>= putInput >> loop

test1' = execExploration alwaysNorth' (initialExplorationState [1..10])
          ^. inputBuilder . to ($ [])

encodeDirection = undefined

move :: Direction -> Exploration Tile
move d = do putInput (encodeDirection d)
            o <- popOutput
            undefined

initialExplorationState :: [Int] -> ExplorationState
initialExplorationState = ES initialTerritory (0, 0) id

findO2 :: [Int] -> [Int]
findO2 p = let outs = unsafeRunIntCompL ins p
               s = execExploration alwaysNorth (initialExplorationState outs)
               ins = ($ []) . _inputBuilder $ s
           in  ins

explore = undefined

initialTerritory :: Territory
initialTerritory = Territory (M.fromList [(( 0, 0), Blank)])
                            $ S.fromList [ ( 1,  0)
                                         , ( 0,  1)
                                         , (-1,  0)
                                         , ( 0, -1) ]

-- instructionsToNextUnknown :: Point -> Territory -> [Int]
-- instructionsToNextUnknown p t = let us = t ^. unexplored . to S.toList
                                    -- target = minimumOn (distanceM p) us
                                -- in findPath t p target

-- movePointSeq :: Point -> [Int] -> Point
-- movePointSeq = foldl' movePoint

-- movePoint :: Point -> Int -> Point
-- movePoint (x, y) 1 = (x, y + 1)
-- movePoint (x, y) 2 = (x, y - 1)
-- movePoint (x, y) 3 = (x - 1, y)
-- movePoint (x, y) 4 = (x + 1, y)
-- movePoint _      _ = error "Unrecognized direction."

-- addNeighboursDeleting :: Point -> Territory -> Territory
-- addNeighboursDeleting p t = let m = t ^. known
                                -- s = S.fromList . filter (`M.notMember` m)
                                                    -- $ movePoint p <$> [1..4]
                            -- in t & unexplored %~ S.union s

-- explore :: [Int] -> (Territory, [Int])
-- explore = go (0, 0) initialTerritory
    -- where go p t outs = let is = instructionsToNextUnknown p t
                            -- (o : outs') = drop (length is - 1) outs
                        -- in case o of
                          -- 0 -> tmap (is ++) $ go p' t' outs'
                                -- where p'  = movePointSeq p $ init is
                                      -- p'' = movePoint p' $ last is
                                      -- t'  = t & known %~ M.insert p'' Wall
                          -- 1 -> tmap (is ++) $ go p' t' outs'
                                -- where p' = movePointSeq p is
                                      -- t' = addNeighboursDeleting p' t
                          -- 2 -> (t', is)
                                -- where p' = movePointSeq p is
                                      -- t' = t & known %~ M.insert p' O2

d15star1 :: Solver
d15star1 f _ = undefined

d15star2 :: Solver
d15star2 f _ = undefined

d15bothStars :: Solver
d15bothStars f _ = undefined
