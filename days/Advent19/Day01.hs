module Advent19.Day01 where

import Advent19.Utils
import Data.List

type Int' = Int

-- |Try computing the result while parsing.

parseInts :: FilePath -> IO [Int']
parseInts = parseFile (lines_ int)

announce' :: Show a => Int -> a -> IO ()
announce' = announce 1

fuelRequirement :: Int' -> Int'
fuelRequirement n | n < 9 = 0
fuelRequirement n = subtract 2 (n `div` 3)

totalFuelRequirement :: Int' -> Int'
totalFuelRequirement n | n < 9 = 0
totalFuelRequirement n = m + totalFuelRequirement m
                              where m = fuelRequirement n

d01star1 :: Solver
d01star1 f _ = announce' 1
          =<< sum . fmap fuelRequirement <$> parseInts f

d01star2 :: Solver
d01star2 f _ = announce' 2
          =<< sum . fmap totalFuelRequirement <$> parseInts f

d01bothStars :: Solver
d01bothStars f _ = display =<< foldl' g (0, 0) <$> parseInts f
                where g (s1, s2) m = let n = fuelRequirement m
                                         n' = totalFuelRequirement n
                                     in (s1 + fromIntegral n
                                        , s2 + fromIntegral (n + n'))
                      display (m, n) = announce' 1 m
                                       >> announce' 2 n
