module Advent19.Day05 where

import Advent19.Utils
import Prelude hiding (LT, EQ)
import Control.Monad
import Control.Monad.Trans.Reader
import Control.Monad.Trans.State
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Data.Array.IO

-- |Make it easy to switch between Int and Integer arrays.
type Int' = Int
type Array' = IOUArray Int Int'

announce' :: Show a => Int -> a -> IO ()
announce' = announce 5

listToArray :: [Int'] -> IO Array'
listToArray [] = fail "Empty list."
listToArray xs = newListArray (0, l) xs where l = length xs - 1

parseProg :: FilePath -> IO Array'
parseProg = listToArray <=< parseFile (line (commaList signedInt))

data Mode = Value | Pointer deriving Show

mode :: Int -> Mode
mode 0 = Pointer
mode 1 = Value

data Instruction = ADD  Mode Mode
                 | MUL  Mode Mode
                 | IN
                 | OUT  Mode
                 | JMPT Mode Mode
                 | JMPF Mode Mode
                 | LT   Mode Mode
                 | EQ   Mode Mode
                 | HALT deriving Show

decodeInstruction :: Int -> Instruction
decodeInstruction n = case digits {-reversed -} of
                        [1,0,x,y,z] -> ADD (mode x) (mode y)
                        [2,0,x,y,z] -> MUL (mode x) (mode y)
                        [3,0,0,0,0] -> IN
                        [4,0,x,0,0] -> OUT (mode x)
                        [5,0,x,y,z] -> JMPT (mode x) (mode y)
                        [6,0,x,y,z] -> JMPF (mode x) (mode y)
                        [7,0,x,y,z] -> LT   (mode x) (mode y)
                        [8,0,x,y,z] -> EQ   (mode x) (mode y)
                        [9,9,0,0,0] -> HALT
                        xs          -> error $ "encountered " ++ show xs
                  where digits = take 5 (toDigitsRev n ++ repeat 0)

type Program = ReaderT (Array', Int) (StateT ([Int], Int) IO)

write' :: Int {- ^ position -} -> Int' {- value -} -> Program ()
write' p n = do (a, _) <- ask
                liftIO $ writeArray a p n

read' :: Int -> Program Int'
read' n = do (a, _) <- ask
             liftIO $ readArray a n

execute :: Program (Maybe Int')
execute = do pos    <- snd <$> lift get
             decodeInstruction <$> read' pos >>= runInstruction

runInstruction :: Instruction -> Program (Maybe Int')
runInstruction (ADD m1 m2)  = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 s  <- lift get
                                 write' a3 (a1 + a2)
                                 lift . put $ (+ 4) <$> s
                                 execute
runInstruction (MUL m1 m2)  = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 s  <- lift get
                                 write' a3 (a1 * a2)
                                 lift . put $ (+ 4) <$> s
                                 execute
runInstruction IN           = do a      <- getArgument 1 Value
                                 (_, n) <- ask
                                 s  <- lift get
                                 write' a n
                                 lift . put $ (+ 2) <$> s
                                 execute
runInstruction (OUT m)      = do res    <- getArgument 1 m
                                 (ress, pos)  <- lift get
                                 lift $ put (res : ress, pos + 2)
                                 execute
runInstruction (JMPT m1 m2) = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 s  <- lift get
                                 let s' = if a1 /= 0
                                            then const (fromIntegral a2) <$> s
                                            else (+ 3) <$> s
                                 lift . put $ s'
                                 execute
runInstruction (JMPF m1 m2) = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 s  <- lift get
                                 let s' = if a1 == 0
                                            then const (fromIntegral a2) <$> s
                                            else (+ 3) <$> s
                                 lift . put $ s'
                                 execute
runInstruction (LT m1 m2)   = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 s  <- lift get
                                 let r = if a1 < a2 then 1 else 0
                                 write' a3 r
                                 lift . put $ (+ 4) <$> s
                                 execute
runInstruction (EQ m1 m2)   = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 s  <- lift get
                                 let r = if a1 == a2 then 1 else 0
                                 write' a3 r
                                 lift . put $ (+ 4) <$> s
                                 execute
runInstruction HALT         = do (ress, _) <- lift get
                                 pure $ safeHead ress

getArgument :: Int -> Mode -> Program Int'
getArgument n m = do (_, pos) <- lift get
                     v <- read' (pos + n)
                     case m of
                       Pointer -> read' v
                       Value   -> pure v

d05star1 :: Solver
d05star1 f _
    = do a                <- parseProg f
         (out, (ress, _)) <- runStateT (runReaderT execute (a, 1)) ([],0)
         let flag = all (== 0) $ tail ress
         case (out, flag) of
           (Just n, True) -> announce' 1 n
           _              -> fail "Diagnostics failed."

d05star2 :: Solver
d05star2 f _
    = do a                <- parseProg f
         (out, (ress, _)) <- runStateT (runReaderT execute (a, 5)) ([],0)
         let flag = all (== 0) $ tail ress
         case (out, flag) of
           (Just n, True) -> announce' 2 n
           _              -> fail "Diagnostics failed."

d05bothStars :: Solver
d05bothStars f f' = d05star1 f f' >> d05star2 f f'
