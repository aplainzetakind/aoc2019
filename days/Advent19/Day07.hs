{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Advent19.Day07 where

import Prelude hiding (LT, EQ)

import Advent19.Utils
import Control.Monad.ST.Lazy (ST, runST)
import Control.Monad.Trans.Reader (ReaderT, runReaderT, ask)
import Control.Monad.Trans.State (StateT, evalStateT, get, put)
import Control.Monad.Trans.Class (lift)
import Data.Array.IO (IOArray, readArray, writeArray, thaw)
import Data.Array (Array, listArray)
import Conduit (ConduitT, runConduit, yield)
import Data.Conduit.Lazy (lazyConsume)
import System.IO.Unsafe (unsafePerformIO)

-- |To switch between ST and IO arrays.
type ArrayType s = IOArray
type ArrayMonad s = IO

type Array' s = (ArrayType s) Int Int

announce' :: Show a => Int -> a -> IO ()
announce' = announce 7

makeArray :: [Int] -> Array Int Int
makeArray l = listArray (0, length l - 1) l

parseInput :: FilePath -> IO (Array Int Int)
parseInput = fmap makeArray . parseFile (line (commaList signedInt))

data Mode = Value | Pointer deriving Show

mode :: Int -> Mode
mode 0 = Pointer
mode 1 = Value

data Instruction = ADD  Mode Mode
                 | MUL  Mode Mode
                 | IN
                 | OUT  Mode
                 | JMPT Mode Mode
                 | JMPF Mode Mode
                 | LT   Mode Mode
                 | EQ   Mode Mode
                 | HALT deriving Show

decodeInstruction :: Int -> Instruction
decodeInstruction n = case digits {-reversed -} of
                        [1,0,x,y,z] -> ADD (mode x) (mode y)
                        [2,0,x,y,z] -> MUL (mode x) (mode y)
                        [3,0,0,0,0] -> IN
                        [4,0,x,0,0] -> OUT (mode x)
                        [5,0,x,y,z] -> JMPT (mode x) (mode y)
                        [6,0,x,y,z] -> JMPF (mode x) (mode y)
                        [7,0,x,y,z] -> LT   (mode x) (mode y)
                        [8,0,x,y,z] -> EQ   (mode x) (mode y)
                        [9,9,0,0,0] -> HALT
                        xs          -> error $ "encountered " ++ show xs
                  where digits = take 5 (toDigitsRev n ++ repeat 0)

data ProgState = PState { _IP   :: Int
                        , _ins  :: [Int]
                        }

newtype Program s a
      = Program
         { getProgram :: ReaderT (Array' s)
                                 (StateT ProgState
                                         (ConduitT () Int (ArrayMonad s))
                                 ) a
                } deriving (Functor, Applicative, Monad)

writeA :: Int {- ^ position -}
       -> Int {- ^ value -}
       -> Program s ()
writeA p n = Program $ do a <- ask
                          lift . lift . lift $ writeArray a p n

readA :: Int -> Program s Int
readA n = Program $ do a <- ask
                       lift . lift . lift $ readArray a n

getProgState :: Program s ProgState
getProgState = Program $ lift get

putProgState :: ProgState -> Program s ()
putProgState s = Program $ lift (put s)

getIP :: Program s Int
getIP = _IP <$> getProgState

putIP :: Int -> Program s ()
putIP n = do s <- getProgState
             putProgState $ s { _IP = n }

modifyIP :: (Int -> Int) -> Program s ()
modifyIP f = getIP >>= putIP . f

getIns :: Program s [Int]
getIns = _ins <$> getProgState

putIns :: [Int] -> Program s ()
putIns ns = do s <- getProgState
               putProgState $ s { _ins = ns }

modifyIns :: ([Int] -> [Int]) -> Program s ()
modifyIns f = getIns >>= putIns . f

popIn :: Program s (Maybe Int)
popIn = do ns <- getIns
           case ns of
             x : xs -> putIns xs >> pure (Just x)
             []     -> pure Nothing

output :: Int -> Program s ()
output n = Program $ lift . lift $ yield n

getArgument :: Int -> Mode -> Program s Int
getArgument n m = do ip <- getIP
                     v <- readA (ip + n)
                     case m of
                       Pointer -> readA v
                       Value   -> pure v

execute :: Program s ()
execute = runInstruction =<< fmap decodeInstruction . readA =<< getIP

runInstruction :: Instruction -> Program s ()
runInstruction (ADD m1 m2)  = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 writeA a3 (a1 + a2)
                                 modifyIP (+ 4)
                                 execute
runInstruction (MUL m1 m2)  = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 writeA a3 (a1 * a2)
                                 modifyIP (+ 4)
                                 execute
runInstruction IN           = do a <- getArgument 1 Value
                                 i <- popIn
                                 case i of
                                    Just n  -> do writeA a n
                                                  modifyIP (+ 2)
                                                  execute
                                    Nothing -> runInstruction HALT
runInstruction (OUT m)      = do getArgument 1 m >>= output
                                 modifyIP (+ 2)
                                 execute
runInstruction (JMPT m1 m2) = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 let f =  if a1 /= 0 then const a2 else (+ 3)
                                 modifyIP f
                                 execute
runInstruction (JMPF m1 m2) = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 let f =  if a1 == 0 then const a2 else (+ 3)
                                 modifyIP f
                                 execute
runInstruction (LT m1 m2)   = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 let r = if a1 < a2 then 1 else 0
                                 writeA a3 r
                                 modifyIP (+ 4)
                                 execute
runInstruction (EQ m1 m2)   = do a1 <- getArgument 1 m1
                                 a2 <- getArgument 2 m2
                                 a3 <- getArgument 3 Value
                                 let r = if a1 == a2 then 1 else 0
                                 writeA a3 r
                                 modifyIP (+ 4)
                                 execute
runInstruction HALT         = pure ()

unsafeRunProgram :: Array Int Int -> [Int] -> [Int]
unsafeRunProgram p is = unsafePerformIO
                          $ do a <- thaw p
                               let producer = getProgram execute
                                                `runReaderT` a
                                                `evalStateT` initialState
                                   initialState = PState 0 is
                               lazyConsume producer

-- :: Array Int Int -> [Int] -> [Int]
-- unsafeRunProgram p ns = runST $ program p ns

perms :: [Int] -> [[Int]]
perms [] = [[]]
perms xs = xs >>= (\x -> (x :) <$> perms (filter (/= x) xs))

phasesToOutputWithFeedback :: Array Int Int -> [Int] -> [Int]
phasesToOutputWithFeedback p xs@[a,b,c,d,e] = runP ei
                        where runP = unsafeRunProgram p
                              ei   = e : od
                              od   = runP di
                              di   = d : oc
                              oc   = runP ci
                              ci   = c : ob
                              ob   = runP bi
                              bi   = b : oa
                              oa   = runP ai
                              ai   = a : 0 : phasesToOutputWithFeedback p xs
phasesToOutputWithFeedback p _
        = error "phasesToOutput: Unexpected list length."

maximumOutput :: Array Int Int -> Int
maximumOutput p = maximum
                  $ take 1 . phasesToOutputWithFeedback p =<< perms [0,1,2,3,4]

loopingMaximum :: Array Int Int -> Int
loopingMaximum p = maximum $ phasesToOutputWithFeedback p =<< perms [5,6,7,8,9]

d07star1 :: Solver
d07star1 f _ = do p <- parseInput f
                  announce' 1 $ maximumOutput p

d07star2 :: Solver
d07star2 f _ = do p <- parseInput f
                  announce' 2 $ loopingMaximum p

d07bothStars :: Solver
d07bothStars f _ = do p <- parseInput f
                      announce' 1 $ maximumOutput p
                      announce' 2 $ loopingMaximum p
