module Advent19.Day12 where

import Advent19.Utils
import Control.Arrow
import Control.Monad ((<=<))
import Data.Tuple.Extra (both, fst3, snd3, thd3)
import Data.Text (pack)
import Data.List (elemIndex)
import Data.Maybe (fromJust)

testInput = [ Moon (P (-1)  0   2) (P 0 0 0)
            , Moon (P  2 (-10) (-7)) (P 0 0 0)
            , Moon (P  4 (-8)   8) (P 0 0 0)
            , Moon (P  3  5  (-1)) (P 0 0 0) ]

announce' :: Show a => Int -> a -> IO ()
announce' = announce 12

parseInput :: FilePath -> IO [Moon]
parseInput = parseFile (lines_ moon)

moon = do string "<x="
          x <- signedInt
          string ", y="
          y <- signedInt
          string ", z="
          z <- signedInt
          string ">"
          pure $ Moon (P x y z) (P 0 0 0)

data Point3D = P { _x :: Int, _y :: Int, _z :: Int } deriving Show

data Moon = Moon { _p :: Point3D
                 , _v :: Point3D } deriving Show

data Axis = Axis { _pos :: Int
                 , _vel :: Int } deriving (Show, Eq)

projections :: Moon -> (Axis, Axis, Axis)
projections (Moon (P px py pz) (P vx vy vz)) = ( Axis px vx
                                               , Axis py vy
                                               , Axis pz vz )

applyGravityAxis :: [Axis] -> [Axis]
applyGravityAxis as = apply as <$> as
                    where apply as (Axis x y) = Axis x y'
                            where y' = y + splitCount x (_pos <$> as)

applyVelocityAxis :: [Axis] -> [Axis]
applyVelocityAxis = fmap apply
            where apply (Axis p v) = Axis (p + v) v

stepAxis :: [Axis] -> [Axis]
stepAxis = applyVelocityAxis . applyGravityAxis

axisPeriod :: [Axis] -> Int
axisPeriod as = (+ 1) . fromJust . elemIndex as . drop 1 . iterate stepAxis
                $ as

period :: [Moon] -> Int
period ms = foldr lcm 1 [periodx, periody, periodz]
              where periodx = axisPeriod (fst3 . projections <$> ms)
                    periody = axisPeriod (snd3 . projections <$> ms)
                    periodz = axisPeriod (thd3 . projections <$> ms)

applyGravity :: [Moon] -> [Moon]
applyGravity ms = apply ms <$> ms
            where apply ms (Moon (P px py pz) (P vx vy vz))
                              = Moon (P px py pz) (P vx' vy' vz')
                    where vx' = vx + splitCount px (_x . _p <$> ms)
                          vy' = vy + splitCount py (_y . _p <$> ms)
                          vz' = vz + splitCount pz (_z . _p <$> ms)

splitCount :: Int -> [Int] -> Int
splitCount n = uncurry subtract . both length . (filter (< n) &&& filter (> n))

applyVelocity :: [Moon] -> [Moon]
applyVelocity = fmap apply
          where apply (Moon (P px py pz) (P vx vy vz))
                          = Moon (P (px + vx) (py + vy) (pz + vz)) (P vx vy vz)

step :: [Moon] -> [Moon]
step = applyVelocity . applyGravity

energy :: Moon -> Int
energy (Moon (P px py pz) (P vx vy vz)) = (abs px + abs py + abs pz)
                                            * (abs vx + abs vy + abs vz)

totalEnergy :: [Moon] -> Int
totalEnergy = sum . fmap energy

d12star1 :: Solver
d12star1 f _ = do moons <- parseInput f
                  announce' 1 . totalEnergy . (!! 1000) $ iterate step moons

d12star2 :: Solver
d12star2 f _ = do moons <- parseInput f
                  announce' 1 . period $ moons

d12bothStars :: Solver
d12bothStars f f' = d12star1 f f' >> d12star2 f f'
