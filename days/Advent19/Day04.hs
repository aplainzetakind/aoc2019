module Advent19.Day04 where

import Advent19.Utils hiding (count)
import Data.Maybe
import Control.Monad.Trans.State
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Class

announce' :: Show a => Int -> a -> IO ()
announce' = announce 4

parseInput :: FilePath -> IO (Int, Int)
parseInput = parseFile $ (,) <$> int <*> (minus *> int)

data Criterion = Star1 | Star2 deriving Eq

data PassState = PS { _digitsLeft :: Int
                    , _prev       :: Maybe Int
                    , _lower      :: [Int]
                    , _upper      :: [Int]
                    , _lastChunk  :: Int
                    , _done       :: Bool
                    } deriving Show

type PassMonad a = StateT PassState (ReaderT Criterion []) a

initialState :: Int -> Int -> PassState
initialState m n = PS dl Nothing l u 0 False
        where u  = reverse . toDigitsRev $ n
              dl = length u
              l  = reverse . take dl $ toDigitsRev m ++ repeat 0

updateState :: Int -> PassMonad ()
{-# INLINE updateState #-}
updateState n = do s <- get
                   if Just n < _prev s
                    then fail "Decresing digits."
                    else do mode <- lift ask
                            let updated = PS lft prv lwr upr lc dne
                                lft = _digitsLeft s - 1
                                prv = Just n
                                lwr = case _lower s of
                                       []   -> []
                                       l:ls -> if n > l then [] else ls
                                upr = case _upper s of
                                       []   -> []
                                       u:us -> if n < u then [] else us
                                lc  = if Just n == _prev s
                                       then _lastChunk s + 1
                                       else 1
                                dne = case mode of
                                        Star1 -> _done s || Just n == _prev s
                                        Star2 -> _done s || _lastChunk s == 2
                                                    && Just n /= _prev s
                            put updated

nextDigits :: PassMonad Int
{-# INLINE nextDigits #-}
nextDigits = do s <- get
                if _digitsLeft s == 0
                  then fail "No more digits to pick."
                  else do mode <- lift ask
                          let choices :: PassState -> [Int]
                              choices s | _digitsLeft s == 1 && not (_done s)
                                          = case mode of
                                             Star1 -> [p]
                                             Star2 -> case _lastChunk s of
                                                        1 -> [p]
                                                        2 -> [l'..u]
                                                        _ -> []
                                where p = fromMaybe 0 $ _prev s
                                      l' = max (p + 1) l
                              choices s | mode == Star2
                                            && _digitsLeft s == 2
                                            && not (_done s)
                                            && _lastChunk s >= 2
                                             = [l'..u]
                                where p = fromMaybe 0 $ _prev s
                                      l' = max (p + 1) l
                              choices s = [l..u]
                              l = fromMaybe 0 . max (_prev s)
                                  $ safeHead (_lower s)
                              u = fromMaybe 9 . safeHead $ _upper s
                          n <- lift . lift $ choices s
                          updateState n
                          pure n

produceDigits :: PassMonad [Int]
produceDigits = do s <- get
                   if _digitsLeft s == 0
                     then pure []
                     else do n <- nextDigits
                             (n :) <$> produceDigits

count :: Int -> Int -> Criterion -> Int
count m n s = length $ evalStateT produceDigits (initialState m n) `runReaderT` s

countBoth :: Int -> Int -> (Int, Int)
countBoth m n = (countOne, countTwo)
      where countOne = count m n Star1
            countTwo = count m n Star2

d04star1 :: Solver
d04star1 f _ = do (m, n) <- parseInput f
                  announce' 1 $ count m n Star1

d04star2 :: Solver
d04star2 f _ = do (m, n) <- parseInput f
                  announce' 2 $ count m n Star2

d04bothStars :: Solver
d04bothStars f _ = do (m, n) <- parseInput f
                      let (r1, r2) = countBoth m n
                      announce' 1 r1
                      announce' 2 r2
