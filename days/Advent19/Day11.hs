module Advent19.Day11 where

import Advent19.Utils
import Advent19.Points2D
import Advent19.IntComp (unsafeRunIntCompL)
import Data.Maybe (fromMaybe)
import qualified Data.Map as M
import Data.List.Extra (chunksOf)
import Data.Tuple.Extra (both)
import Control.Arrow

announce' :: Show a => Int -> a -> IO ()
announce' = announce 11

parseInput :: FilePath -> IO [Int]
parseInput = parseFile (line (commaList signedInt))

type CompIn  = [Int]
type CompOut = [Int]

data Color       = Black | White deriving Eq
data RobotState  = RS { _pos           :: Point
                      , _bearing       :: Direction } deriving Show
type PanelState  = M.Map Point Color
data SystemState = SS { _robotState :: RobotState
                      , _panelState :: PanelState
                      }

scanCoordColor :: RobotState -> PanelState -> Color
scanCoordColor (RS p _) ps = fromMaybe Black $ M.lookup p ps

initialRobotState :: RobotState
initialRobotState = RS (0, 0) U

turn :: Int -> Direction -> Direction
turn 0 R = U
turn 0 U = L
turn 0 L = D
turn 0 D = R
turn 1 R = D
turn 1 D = L
turn 1 L = U
turn 1 U = R
turn x _ = error $ "Invalid turn output: " ++ show x

paint :: Int -> Color
paint 0 = Black
paint 1 = White

initialState :: SystemState
initialState = SS (RS (0, 0) U) M.empty

stepState :: [Int] -> SystemState -> (SystemState, Int)
stepState [p, t] (SS (RS pos bear) ps) = (SS (RS pos' bear') ps', i)
                      where bear' = turn t bear
                            pos'  = movePoint bear' 1 pos
                            ps'   = M.insert pos (paint p) ps
                            i     = case scanCoordColor (RS pos' bear') ps' of
                                      Black -> 0
                                      White -> 1

consumeOutput' :: (SystemState, Int) -> [[Int]] -> (SystemState, [Int])
consumeOutput' s@ ~(ss, i) xs
    = tmap (i :) $ case xs of
            []             -> (ss, [])
            [p, t] : rest  -> consumeOutput' (stepState [p, t] ss) rest

consumeOutput :: (SystemState, Int) -> [Int] -> (SystemState, [Int])
consumeOutput s = consumeOutput' s . chunksOf 2

runRobot :: Int -> [Int] -> (SystemState, [Int])
runRobot c p = let ins   = snd res
                   outs = unsafeRunIntCompL ins p
                   res   = consumeOutput (initialState, c) outs
               in  res

getPanelRanges :: PanelState -> ((Int, Int), (Int, Int))
getPanelRanges m = let (xm, ym) = (M.mapKeys fst &&& M.mapKeys snd) m
                   in both (fst . M.findMin &&& fst . M.findMax) (xm, ym)

visualize :: PanelState -> String
visualize m = unlines [ [ f i j | i <- [xm..xM] ] | j <- [yM, yM - 1..ym] ]
                where f i j = if M.member (i, j) m' then '▓' else ' '
                      ((xm, xM), (ym, yM)) = getPanelRanges m'
                      m' = M.filter (== White) m

d11star1 :: Solver
d11star1 f _ = do p <- parseInput f
                  announce' 1 . M.size . _panelState . fst $ runRobot 0 p

d11star2 :: Solver
d11star2 f _ = do p <- parseInput f
                  putStrLn "Day 11 star 2:\n"
                  putStrLn . visualize . _panelState . fst $ runRobot 1 p

d11bothStars :: Solver
d11bothStars f f' = d11star1 f f' >> d11star2 f f'
