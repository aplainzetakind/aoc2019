{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}
module Advent19.Day13 where

import Advent19.Utils hiding (State)
import Advent19.Points2D
import Advent19.IntComp (unsafeRunIntCompL)
import qualified Data.Map as M
import qualified Data.Set as S
import Data.List.Extra (chunksOf, minimumOn)
import Control.Monad.State.Class
import Control.Monad.Trans.State
import Lens.Micro hiding (both)
import Lens.Micro.Mtl
import Lens.Micro.TH
import Data.Tuple.Extra (both)
import Control.Arrow
import Debug.Trace
import Data.Maybe
import Text.Printf

-- No need to keep the entire screen for part 2.

announce' :: Show a => Int -> a -> IO ()
announce' = announce 13

parseInput :: FilePath -> IO [Int]
parseInput = parseFile (line (commaList signedInt))

data Tile = Empty
          | Wall
          | Block
          | Paddle
          | Ball deriving (Eq, Show)

data Object = Object { _point :: Point
                     , _tile  :: Tile } deriving Show

makeLenses ''Object

data Screen = Screen { _blanks  :: S.Set Point
                     , _walls   :: S.Set Point
                     , _blocks  :: S.Set Point
                     , _paddles :: S.Set Point
                     , _balls   :: S.Set Point } deriving Show

makeLenses ''Screen

tileLens :: Tile -> Lens' Screen (S.Set Point)
tileLens Empty = blanks
tileLens Wall = walls
tileLens Block = blocks
tileLens Paddle = paddles
tileLens Ball = balls

removePoint :: Point -> Screen -> Screen
removePoint p (Screen bks ws bcs ps bs) = Screen (S.delete p bks)
                                                 (S.delete p ws)
                                                 (S.delete p bcs)
                                                 (S.delete p ps)
                                                 (S.delete p bs)

objectToScreen :: Object -> Screen -> Screen
objectToScreen (Object p t) sc = removePoint p sc & tileLens t %~ S.insert p

emptyScreen :: Screen
emptyScreen = Screen S.empty S.empty S.empty S.empty S.empty

tileCount :: Tile -> Screen -> Int
tileCount t = S.size . view (tileLens t)

data GameState = GS { _screen  :: Screen
                    , _paddlex :: Maybe Int
                    , _ballx   :: Maybe Int
                    , _pmove    :: Maybe Int
                    , _score   :: Maybe Int }

makeLenses ''GameState

instance Show GameState where
  show gs = "\n" ++ visualize gs

visualize :: GameState -> String
visualize gs = unlines [ [ f i j | i <- [xm..xM] ] | j <- [ym..yM] ]
                where f i j = renderTile gs (i, j)
                      ((xm, xM), (ym, yM)) = getScreenRanges s'
                      s' = gs ^. screen

renderTile :: GameState -> Point -> Char
renderTile gs p | S.member p (gs ^. screen . balls) = 'o'
renderTile gs p | S.member p (gs ^. screen . walls) = '▓'
renderTile gs p | S.member p (gs ^. screen . blocks) = '#'
renderTile gs p | S.member p (gs ^. screen . paddles) = '-'
renderTile gs p | S.member p (gs ^. screen . blanks) = ' '
renderTile _ _ = ' '

getScreenRanges :: Screen -> ((Int, Int), (Int, Int))
getScreenRanges (Screen p q r s t)
          = let (xm, ym) = (S.map fst &&& S.map snd) m
                m = foldr S.union S.empty [p,q,r,s,t]
            in both (S.findMin &&& S.findMax) (xm, ym)

initialState :: GameState
initialState = GS emptyScreen Nothing Nothing Nothing Nothing

updateScore :: Int -> Game ()
updateScore n = score ?= n

putObject :: Object -> Game ()
putObject o = screen %= objectToScreen o

newtype Game a = Game {_getGame :: State GameState a}
            deriving (Functor, Applicative, Monad, MonadState GameState)

execGame :: Game a -> GameState -> GameState
execGame g = execState (_getGame g)

data JoystickPosition = JL | JN | JR

toTile :: Int -> Tile
toTile 0 = Empty
toTile 1 = Wall
toTile 2 = Block
toTile 3 = Paddle
toTile 4 = Ball
toTile _ = error "Invalid tile"

chunkToObject :: [Int] -> Object
chunkToObject [x,y,t] = Object (x, y) $ toTile t

chunkExec :: [Int] -> Game ()
chunkExec [-1, 0, n] = updateScore n
chunkExec xs@[x,y,t] = putObject (chunkToObject xs)
                       >> case t of
                            3 -> paddlex ?= x >> pmove .= Nothing
                            4 -> do ballx ?= x
                                    px <- use paddlex
                                    pmove .= (signum . (`subtract` x) <$> px)
                            _ -> pmove .= Nothing

outputToStates :: [Int] -> Screen
outputToStates = view screen . (`execGame` initialState) . traverse chunkExec
                                                         . chunksOf 3

intermediateStates :: GameState -> [Game a] -> [GameState]
intermediateStates s [] = []
intermediateStates s (g : gs) = let s' = execGame g s
                                in  s' : intermediateStates s' gs

autoJoy :: GameState -> Maybe Int
autoJoy = (^. pmove)

insertCoins :: Int -> [Int] -> [Int]
insertCoins n (x : xs) = n : xs
insertCoins _ []       = error "No game found."

arkanoidScore :: [Int] -> Maybe Int
arkanoidScore p = let outs       = chunksOf 3 . unsafeRunIntCompL ins
                                                          $ insertCoins 2 p
                      ins        = 0 : mapMaybe autoJoy states
                      states     = intermediateStates initialState
                                                        $ fmap chunkExec outs
                      finalscore = last states ^. score
                  in  finalscore

d13star1 :: Solver
d13star1 f _ = do p <- parseInput f
                  announce' 1 . tileCount Block . outputToStates
                              $ unsafeRunIntCompL [] p

d13star2 :: Solver
d13star2 f _ = do p <- parseInput f
                  announce' 1 . fromJust $ arkanoidScore p

d13bothStars :: Solver
d13bothStars f f' = d13star1 f f' >> d13star2 f f'
